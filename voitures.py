# coding: utf-8
# import flask_login 
# import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')

from flask import Flask, render_template, redirect, url_for, request, flash, session, g,jsonify
import psycopg2
import datetime
import re  # verification des mails, noms, etc
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash, check_password_hash
import datetime
from PIL import Image
# from resizeimage import resizeimage

import os
from werkzeug.utils import secure_filename
import pandas as pd
import numpy as np
from flask_paginate import Pagination, get_page_args
from flask_sqlalchemy import SQLAlchemy

from flask_cors import CORS
from sqlalchemy import or_
# from flask_caching import Cache




# 1 - imports tables of data base
from datetime import date

from basevoiture import Sessionalchemy, engine, Base
from marques import Marques
from mycars import Modele
from users import User


# 2 - generer le shema la base de données
Base.metadata.create_all(engine)
######################################FLASK MIGRATE###################################
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
#######################################################################################



###################### CREATION DE MOT DE PASS ALEATOIRE ##################
import string
from random import sample

app = Flask(__name__)
CORS(app)

# define the cache config keys, remember that it can be done in a settings file
app.config['CACHE_TYPE'] = 'simple'
# register the cache instance and binds it on to your app 
# app.cache = Cache(app) 

HOSTNAME = '127.0.0.1'
PORT = '5432'
DATABASE = 'africmode'
USERNAME = 'postgres'
PASSWORD = 'admin'


app.config["IMAGE_UPLOADS"] ="/home/mamadou/Bureau/PROJETS/voitures/projet_auto/static/images"
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF", "tiff", "HEIF", "HEIC"]
ALLOWED_EXTENSIONS = set([ 'png', 'jpg', 'jpeg', 'gif', 'tiff', 'heif', 'heic'])

app.config["MAX_IMAGE_FILESIZE"] = 0.5 * 4050 * 4050
app.config['SQLALCHEMY_DATABASE_URI']="postgres+psycopg2://{}:{}@{}:{}/{}".format (USERNAME,PASSWORD,HOSTNAME,PORT,DATABASE)
Sessionalchemy = Sessionalchemy()

db = SQLAlchemy(app)

migrate = Migrate(app,db)
manager = Manager(app)
manager.add_command('db',MigrateCommand)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_image(filename):
    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]
    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False

def allowed_image_filesize(filesize):

    if int(filesize) <= app.config["MAX_IMAGE_FILESIZE"]:
        return True
    else:
        return False



### Add marques 

marque_voiture = {"nom": "voiture", "marque":["Toyota", "Mercedes-benz", "BMW", "Ford", "Honda", "Nissan","Renault", "Peugeot", "Citroën C3", "Dacia", "Volkswagen", "volvo", "mazda", "suzuki", "kia", "hyundai", "Mitsubishi", "Autres"]}
marque_moto = {"nom":"moto", "marque":[ "Tmax", "Suzuki", "Jakarta", "Scooter", "Beverly", "Yamaha", "Piaggio", "Peageot", "Honda", "Liberty", "Autres"]}
piece_voiture = {"nom":"piecevoiture", "marque":["Ampoules", "Plaquettes de frein", "Pneus", "Carrosserie", "Amortissement", "Climatisation", "Direction", "Echappement", "Embrayage","Moteur", "Autres"]}
piece_moto = {"nom":"piecemoto", "marque":["Échappement moto", "Freinage Moto", "Filtres moto", "Pneus et roues", "Amortisseur", "Batteries", "pieces moteur et cables", "Habillages protection moto", "Eclairage et signalisation", "Casque", "Autres"]}

marquess = [marque_voiture, marque_moto, piece_voiture, piece_moto]

mar = Sessionalchemy.query(Marques)\
    .count()

if mar < 51:
    for i in marquess:
        nom = i["nom"]
        marque = i["marque"]
        for j in marque:
            marq = Marques(nom, j)
            Sessionalchemy.add(marq)
            Sessionalchemy.commit()




## Add users

## Admin Khadim
id_kha = 1
prenom_kha = "Khadim"
nom_kha = "NGING"
telephone_kha = "771846968"
adresse_kha = "Camberen"
mail_kha = "khadimracky92@gmail.com"
pass_word_kha = "khadim@voiture2020"
pass_wordx_kha = generate_password_hash(pass_word_kha)


## Admin SARR
id_sar = 2
prenom_sar = "Mamadou"
nom_sar = "SARR"
telephone_sar = "777886847"
adresse_sar = "Guédiawaye"
mail_sar = "mamadoutive@yahoo.fr"
role = "admin"
pass_word = "mamadou@voiture2020"
pass_wordx_sar = generate_password_hash(pass_word)
etat = "valide"
date_inscription = str(datetime.datetime.now())

## Admin vendeurnomvendeur fictif
id_baba = 3
prenom_baba = "Baba"
nom_baba = "Mal"
telephone_baba = "777886848"
adresse_baba = "Guédiawaye"
mail_baba = ""
role_baba = "vendeur"
pass_word = "babamal2020"
pass_wordx_baba = generate_password_hash(pass_word)
etat = "valide"
# boutique_baba = "Baba Couture"
date_inscription = str(datetime.datetime.now())

## Admin Ndiaga
id_ndia = 4
prenom_ndia = "Ndiaga"
nom_ndia = "GAYE"
telephone_ndia = "771817731"
adresse_ndia = "Thies"
mail_ndia = "gaye.ndiaga@ugb.edu.sn"
pass_word_ndia = "ndiaga@voiture2020"
pass_wordx_ndia = generate_password_hash(pass_word_ndia)
etat = "valide"
date_inscription = str(datetime.datetime.now())



exits_kha = Sessionalchemy.query(User.id).filter(User.telephone==telephone_kha).scalar()
exits_sar = Sessionalchemy.query(User.id).filter(User.telephone==telephone_sar).scalar()
exits_ndia = Sessionalchemy.query(User.id).filter(User.telephone==telephone_ndia).scalar()
exits_baba = Sessionalchemy.query(User.id).filter(User.telephone==telephone_baba).scalar()

if exits_kha == None and exits_sar == None and exits_ndia == None and exits_baba == None:
    use_kha = User(prenom_kha, nom_kha, telephone_kha, adresse_kha, mail_kha, pass_wordx_kha, role, etat, date_inscription)
    use_sar = User(prenom_sar,  nom_sar, telephone_sar, adresse_sar, mail_sar, pass_wordx_sar, role, etat, date_inscription)
    use_ndia = User(prenom_ndia,  nom_ndia, telephone_ndia, adresse_ndia, mail_ndia, pass_wordx_ndia, role, etat, date_inscription)
    use_baba = User(prenom_baba, nom_baba, telephone_baba, adresse_baba, mail_baba, pass_wordx_baba, role_baba, etat, date_inscription)

    Sessionalchemy.add(use_kha)
    Sessionalchemy.add(use_sar)
    Sessionalchemy.add(use_ndia)
    Sessionalchemy.add(use_baba)
    Sessionalchemy.commit()



################################### LOGIN #################################

#### code pour generer un secret key: python -c 'import os; print(os.urandom(16))'

app.secret_key = "b\x062AA\xe5A\xa7?\t*%\x1a'9\x9f\xd2"

# login_manager = flask_login.LoginManager()
# login_manager.init_app(app)

idd = []


@app.route('/login/', methods=['GET', 'POST'])
def login():
    count=0
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'telephon' in request.form and 'password' in request.form :
      # Create variables for easy access
        telephon = request.form['telephon']
        pass_wordd = request.form['password']
        hash = []
        exits = Sessionalchemy.query(User.id).filter(User.telephone==telephon).scalar()
        if exits != None:
            data = Sessionalchemy.query(User)\
                .filter(User.telephone == telephon)\
                .all()
            
            for i in data:
                pass_word = i.pass_word
                hash.append(pass_word)
            # pass_words = session.get(pass_word, None)
            val1 = check_password_hash(hash[0], pass_wordd)
            if val1 == True:
            
                # etat_pwd = request.form['etat_pwd']

                person = Sessionalchemy.query(User)\
                    .filter(User.telephone == telephon)\
                    .filter(User.pass_word == hash[0])\
                    .all()
                
                account = []
                if (telephon!=771846968  and pass_wordd!='khadim@voiture2020') and (telephon!=777886847  and pass_wordd!='mamadou@voiture2020') and (telephon!=771817731  and pass_wordd!='ndiaga@voiture2020'):
                   
                    for i in person:
                        id = i.id
                        nom = i.nom
                        # boutique = i.boutique
                        telephone = i.telephone
                        adresse = i.adresse
                        account.append(id)
                        account.append(nom)
                        # account.append(boutique)
                        account.append(telephone)
                        account.append(adresse)
                    # for i in account:
                    # Create session data, we can access this data in other routes
                    session['loggedin'] = True
                    session['id'] = account[0]
                    session['telephon'] = account[2]
                    # Redirect to home page

                    count +=1    
                    return redirect(url_for('modele'))
                else:
                   
                    for i in person:
                        id = i.id
                        nom = i.nom
                        # boutique = i.boutique
                        telephone = i.telephone
                        adresse = i.adresse
                        account.append(id)
                        account.append(nom)
                        # account.append(boutique)
                        account.append(telephone)
                        # account.append(adresse)
                    # for i in account:
                        # Create session data, we can access this data in other routes
                    session['loggedin'] = True
                    session['id'] = account[0] 
                    session['telephon'] = account[2]
                    return redirect(url_for('chef'))
            else:
                flash("Le mot de pass tapé est incorrect!", "danger")
        else:
            flash("Le numéro téléphone est incorrect!", "danger")                        
    return render_template('account.html')



# Page Accueil

@app.route('/')
# @app.cache.cached(timeout=3600)     # mettre en cache chaque 1h
def accueil():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page

    vendeur=[]
    id_user = session.get('id',None)

    modes = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo == 'valide')\
        .all()
    vends = []
    for i in modes:   
        photo = i.photo
        marque = i.marque
        annee_modele = i.annee_modele
        type_carburant = i.type_carburant
        kilometre = i.kilometre
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nom = i.nom
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        adresse = i.adresse
        id = i.id
        categorie = i.categorie
        objet = i.objet
        vends.append((photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nom, nomvendeur, telephone, adresse, id, categorie, objet))

    for i in vends:
        L1=list(i)
        L1[0]=L1[0].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
                                             
    total = len(vends)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('index.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


# popop images
@app.route('/&<string:id_model>', methods = ['GET','POST'])
def detail_popup(id_model):
    sql =Sessionalchemy.query(Modele)\
        .filter(Modele.id == id_model)\
        .all()
    
    for val in sql:
        tail = val.photo
    vendeur =[]
    L1=list(tail[0])
    L1[0]=L1[0].split('&-&')

    mon_dict = {"img":L1[0]}
    vendeur.append(mon_dict)
  
    return jsonify(vendeur)
####################### Partie détail ##############################
@app.route('/detail&<string:id_ap>', methods=['GET'])
def detail(id_ap):
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    vendeur=[]
    modes = Sessionalchemy.query(Modele)\
        .filter(Modele.id==id_ap)\
        .filter(Modele.etat_photo == 'valide')\
        .all()
    tell = []
    for i in modes:  
        id = i.id 
        photo = i.photo
        marque = i.marque
        annee_modele = i.annee_modele
        type_carburant = i.type_carburant
        kilometre = i.kilometre
        nbr_portes = i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nom = i.nom
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        adresse = i.adresse
        categorie = i.categorie
        descrip = i.description
        tell.append((id, telephone, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nom, nomvendeur, adresse, categorie, descrip))
    L1=list(tell[0])
    L1[2]=L1[2].split('&-&')
    tel=tuple(L1)
    # print(tel)
    valeurs = Sessionalchemy.query(Modele)\
        .filter(Modele.marque==tel[3])\
        .filter(Modele.etat_photo == 'valide')\
        .all()
    tof = []                 
    for i in valeurs:   
        id = i.id
        photo = i.photo
        marque = i.marque
        annee_modele = i.annee_modele
        type_carburant = i.type_carburant
        kilometre = i.kilometre
        nbr_portes = i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nom = i.nom
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        adresse = i.adresse
        description = i.description
        objet = i.objet
        tof.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nom, nomvendeur, telephone, adresse, description, objet))
    
    # det=tof[1].split('&-&')
    tail=tof[::-1]

    for i in tail:
        L1=list(i)
        L1[1]=L1[1].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)  
    total = len(tail)
    # print(vendeur)
    pagination_users=vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')

    return render_template('detail.html',users=pagination_users, page=page, per_page=per_page,pagination=pagination, tel=tel)

  


# #####*********************************************barre de recherche****************************************************#########
# @app.route('/search/<val>')
@app.route('/search', methods=['GET', 'POST'])
def search():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if request.method == "POST":
        vall=request.form.get('vals')       
        val=vall.lower()
        search = "%{}%".format(val)
        valeurs = Sessionalchemy.query(Modele)\
            .filter(or_(Modele.marque.ilike(search), Modele.nom.ilike(search), Modele.annee_modele.ilike(search), Modele.type_carburant.ilike(search), Modele.boite_vitesse.ilike(search), Modele.nom_vendeur.ilike(search), Modele.adresse.ilike(search), Modele.telephone.ilike(search)))\
            .all()

        datas = []
        for i in valeurs:
            id = i.id
            photo = i.photo
            marque = i.marque
            annee_modele = i.annee_modele
            type_carburant = i.type_carburant
            kilometre = i.kilometre
            nbr_portes = i.nbr_portes
            boite_vitesse = i.boite_vitesse
            prix = i.prix
            nom = i.nom
            mail = i.mail
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            adresse = i.adresse
            description = i.description
            objet = i.objet
            datas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))

            # datas.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nom, nomvendeur, telephone, adresse, description, objet))

        data=datas[::-1]
        for i in data:
            L1=list(i)
            L1[3]=L1[3].split('&-&')
            T1=tuple(L1)
            searchh.append(T1)
        total = len(data)
        pagination_users=searchh[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')

    return render_template('searchvendeur.html',users=pagination_users, page=page, per_page=per_page,pagination=pagination)



################################### Inscription du vendeur #################################


@app.route('/Inscription_vendeur', methods=['GET', 'POST'])
def Inscriptionvendeurs():
    msggz=''
    
    if request.method == 'POST' and 'prenom' in request.form and 'telephone' in request.form and 'telephone2' in request.form and 'adresse' in request.form and 'password' in request.form:
        prenom = request.form.get("prenom")
        nom = request.form.get("nom")
        telephone = request.form.get("telephone")
        telephone2 = request.form.get("telephone2")

        adresse = request.form.get("adresse")
        mail = request.form.get("mail")
        pass_wordd = request.form.get('password')
        pass_wordd22 = request.form.get('password22')
        if pass_wordd == pass_wordd22:
            if len(pass_wordd) >=6:
                pass_word = generate_password_hash(pass_wordd) #hachage password
                role = 'vendeur'
                niveau = 'niveau1'
                date_inscription = str(datetime.datetime.now())
                etat = 'valide'
                if telephone == telephone2:
                    exits = Sessionalchemy.query(User.id).filter(User.telephone==telephone).scalar()
  
                    if exits == None:
                        users = User(prenom, nom, telephone, adresse, mail, pass_word, role, etat, date_inscription)
                        Sessionalchemy.add(users)
                        Sessionalchemy.commit()

                        flash("Felicitation, vous avez créé votre compte avec succés!", "succes")

                        return redirect(url_for('modele'))
                        
                    else:
                        flash("Le numéro de telephone existe déjà!","danger")
                else:                    
                    flash("Vérifiez le numéro de téléphone saisi svp!","danger") 
            else:
                flash("Le mot de passe saisi est trés faible, augmentez le nombre au minimum 6 charactéres.","danger")
        else:
            flash("Les mots de passes ne sont pas identiques!","danger") 

    # Show registration form with message (if any) 
    return render_template('creating.html')



    ################################### Enregistrement du Modele #################################

@app.route('/modele', methods=['GET', 'POST'])
# @app.cache.cached(timeout=3600)     # mettre en cache par 1h

def modele():
    photos=[]
    im=[]
    dd=""
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if session.get('id'):
        id_user = session.get('id',None)
        nom_user = Sessionalchemy.query(User.nom)\
            .filter_by(id=id_user).first()

        # userr = Sessionalchemy.query(User)\
        #         .all()

        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo=='valide')\
            .filter(Modele.user_id==id_user)\
            .all()

        pho = []
        for i in valeurs:
            id = i.id
            marque = i.marque
            annee_modele = i.annee_modele
            type_carburant = i.type_carburant
            kilometre = i.kilometre
            nbr_portes = i.nbr_portes
            boite_vitesse = i.boite_vitesse
            photo = i.photo
            prix = i.prix
            categorie = i.categorie
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            mail = i.mail
            adresse = i.adresse
            # boutique = i.boutique
            pho.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, categorie, nomvendeur, telephone, mail, adresse))

        for i in pho:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            photos.append(T1)
        # if request.method == 'POST': #and 'options' in request.form and 'choix' in request.form :
        #     # photoo = request.form.get("photo")
        #     nom = request.form.get("options")
        #     marque = request.form.get("choix")
        #     annee_model = request.form.get("annee")
        #     type_carb = request.form.get("typecarburant")
        #     km = request.form.get("km")
        #     nbr_port = request.form.get("nbr_port")
        #     prixx = request.form.get("prix")
        #     genre = request.form.get("options")
        #     boitvit = request.form.get("boitvit")
        #     description = request.form.get("description")

        #     use = Sessionalchemy.query(User)\
        #         .filter(User.id == id_user)\
        #         .all()
            
        #     user = []
        #     for i in use:
        #         nom = i.nom
        #         telephone = i.telephone
        #         adresse = i.adresse
        #         mail = i.mail
        #         user.append((nom, telephone, adresse, mail))

        #     nom = user[0][0] #request.form.get("nomPerson")
        #     nomPerson=nom
        #     telephone = user[0][1] #request.form.get("telephone")
        #     adresse = user[0][2] #request.form.get("adresse")            
        #     etat_photo = 'invalide' 
        #     mail = user[0][3]
        #     dateajout = str(datetime.datetime.now())

        #     # # ***********************************     
        #     if request.files:

        #         images = request.files.getlist("image")
        #         if len(images)>2:
        #             flash("Vous êtes autorisé à télécharger au maximum 2 photos.","danger")
        #         else:
        #             for image in images:
        #                 if image.filename == "":
        #                     flash("Entrez svp une image", "avertissement")
        #                     return redirect(request.url)

        #                 if allowed_image(image.filename):
        #                     filename = secure_filename(image.filename)
                            
        #                     filenam, file_extension = os.path.splitext(filename)
                            
        #                     hash = generate_password_hash(filenam)
        #                     hash=hash[-10:]
        #                     d = hash+file_extension
        #                     im.append(d)    
        #                     image.save(os.path.join(app.config["IMAGE_UPLOADS"], d))
        #             if len(im)==2:
        #                 dd= im[0]+'&-&'+im[1]
        #             else:
        #                 dd= im[0]+'&-&'+im[0]
        #             photoo =dd
        #             user = Sessionalchemy.query(User).filter_by(id=id_user).first()
        #             print(user)
        #             #user = [val for val in user][0]
        #             models = Modele(nom, marque, photoo, annee_model, type_carb, km, nbr_port, boitvit, prixx, etat_photo, dateajout, None, nomPerson, telephone, mail, adresse, description, objet, user)
        #             Sessionalchemy.add(models)
        #             Sessionalchemy.commit()
                    
        #             flash("Felicitation, vous avez ajouté un modéle avec succés, merci de patienter pour la validation de notre équipe.","succes")   

        #         # else:
        #         #     return " not in request.cookie" #redirect(request.url)

        #     return redirect(request.url)  
        
        total = len(photos)
        pagination_users=photos[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')
    
    else:
        return redirect(url_for('login'))
    
    return render_template('index1.html',users=pagination_users, page=page, per_page=per_page,pagination=pagination)


####################### boutons en en attende toutes validés #####################################

@app.route('/option&<string:id_option>', methods=['GET','POST'])
def option(id_option):
    valeurs = Sessionalchemy.query(Marques)\
    .filter(Marques.nom==id_option)\
    .distinct()

    liste_res = []
    for i in valeurs:   
        liste_res.append({"marque":i.marque})
    # print(liste_res)

    return jsonify(liste_res)


@app.route('/supprim/<string:id_ap>', methods=['GET'])
def supprim(id_ap):
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==4:

        update = Sessionalchemy.query(Modele)\
            .filter(Modele.id==id_ap).first()
        update.etat_photo = 'user_supprime'
        Sessionalchemy.commit()
        flash("La commande a été supprimée.","succes")
    else:
        return redirect(url_for('login')) 

    return redirect(url_for('modele'))

@app.route('/ajoutmodel', methods=['GET', 'POST'])
def ajoutmodel():
    
    return render_template('popop.html')

@app.route('/ajoumodeluser', methods=['GET', 'POST'])
def ajoumodeluser():
    return render_template('border.html')


@app.route('/filtre', methods=['GET', 'POST'])
def filtre():
    if request.method == 'POST':
        page = int(request.args.get('page', 1))
        per_page = 20
        offset = (page - 1) * per_page
        categories = request.form.get("categories")
        modell = request.form.get("modell")
        annee = request.form.get("annee")
        carburant = request.form.get("carburant")
        valeurs = Sessionalchemy.query(Modele)\
                .filter(Modele.etat_photo== 'valide')\
                .filter(Modele.categorie== categories)\
                .all()
        df = pd.DataFrame([(d.id, d.nom, d.photo, d.prix, d.nom_vendeur, d.telephone, d.adresse, d.mail, d.annee_modele, d.type_carburant, d.kilometre, d.nbr_portes, d.boite_vitesse, d.description) for d in valeurs], 
                    columns=['id', 'nom', 'photo', 'prix', 'nom_vendeur', 'telephone', 'adresse', 'mail', 'annee_modele', 'type_carburant', 'kilometre', 'nbr_portes', 'boite_vitesse', 'description'])
        
        c1 = [modell, annee, carburant]

        df2 = df[df.isin(c1).any(axis=1)]

        Row_list =[] 
        for index, rows in df2.iterrows(): 
            # append the tuple to the final list 
            Row_list.append((rows.id, rows.nom, rows.photo, rows.prix, rows.nom_vendeur, rows.telephone, rows.telephone, rows.adresse, rows.mail, rows.mail, rows.annee_modele, rows.type_carburant, rows.kilometre, rows.nbr_portes, rows.boite_vitesse, rows.description)) 
        liste = []
        for i in Row_list:
            L1=list(i)
            L1[2]=L1[2].split('&-&')
            T1=tuple(L1)
            liste.append(T1)
        # Print the list 

        total = len(liste)
        paginationUsers = liste[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    
    
    return render_template('filtre.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/vente', methods=['GET', 'POST'])
def vente():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    
    valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'valide')\
            .filter(Modele.objet== 'vente')\
            .all()
    toyotas = []
    vendeur = []
    for i in valeurs:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        ventes = i.objet
        
        toyotas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet, ventes))
    # if len(toyotas) != 0:
    for i in toyotas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(toyotas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')


    
    return render_template('vente.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/location', methods=['GET', 'POST'])
def location():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    
    valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'valide')\
            .filter(Modele.objet== 'location')\
            .all()
    toyotas = []
    vendeur = []
    for i in valeurs:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        locations = i.objet
        
        toyotas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet, locations))
    # if len(toyotas) != 0:
    for i in toyotas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(toyotas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')


    
    return render_template('location.html', users=paginationUsers, page=page, per_page=per_page,pagination=pagination)




####################################### PARTIE CHEF ###############################


@app.route('/borderadmin', methods=['GET', 'POST'])
def borderadmin():
    
    return render_template('borderadmin.html')


@app.route('/voiturevalide', methods=['GET', 'POST'])
def voiturevalide():
    count=0
    liste = []
    listeUsers = []
    userTrue =[]
    nous=[]
    new_photos=[]
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==4:
        
        # Selection et affiche des photos dont leurs etats=false
        comptes = Sessionalchemy.query(User)\
            .distinct()\
            .count()
        new_phot = []
        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'valide')\
            .filter(Modele.categorie== 'voiture')\
            .all()

        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            adresse = i.adresse

            new_phot.append((id, photo, nom, prix, nomvendeur, telephone, adresse))
        x=session.get('id')
        valeurs = Sessionalchemy.query(User)\
            .filter(User.id== x)\
            .all()
        moi = []
        for i in valeurs:
            nom = i.nom
            prenom = i.prenom
            moi.append((prenom, nom))
              
        for i in new_phot:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            new_photos.append(T1)

        
        total = len(new_photos)
        pagination_users=new_photos[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')
    
    else:
        return redirect(url_for('login')) 
    
    return render_template('voiturevalide.html', users=pagination_users, page=page, per_page=per_page,pagination=pagination,comptes=comptes, moi=moi)


@app.route('/motovalide', methods=['GET', 'POST'])
def motovalide():
    count=0
    liste = []
    listeUsers = []
    userTrue =[]
    nous=[]
    new_photos=[]
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==4:
        
        # Selection et affiche des photos dont leurs etats=false
        comptes = Sessionalchemy.query(User)\
            .distinct()\
            .count()
        new_phot = []
        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'valide')\
            .filter(Modele.categorie== 'moto')\
            .all()

        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            adresse = i.adresse

            new_phot.append((id, photo, nom, prix, nomvendeur, telephone, adresse))
        x=session.get('id')
        valeurs = Sessionalchemy.query(User)\
            .filter(User.id== x)\
            .all()
        moi = []
        for i in valeurs:
            nom = i.nom
            prenom = i.prenom
            moi.append((prenom, nom))
              
        for i in new_phot:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            new_photos.append(T1)

        
        total = len(new_photos)
        pagination_users=new_photos[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')
    
    else:
        return redirect(url_for('login')) 
    
    return render_template('motovalide.html', users=pagination_users, page=page, per_page=per_page,pagination=pagination,comptes=comptes, moi=moi)

    

@app.route('/piecevalide', methods=['GET', 'POST'])
def piecevalide():
    count=0
    liste = []
    listeUsers = []
    userTrue =[]
    nous=[]
    new_photos=[]
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    searchh=[]
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==4:
        
        # Selection et affiche des photos dont leurs etats=false
        comptes = Sessionalchemy.query(User)\
            .distinct()\
            .count()
        new_phot = []
        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'valide')\
            .filter(Modele.categorie== 'piece')\
            .all()

        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            adresse = i.adresse

            new_phot.append((id, photo, nom, prix, nomvendeur, telephone, adresse))
        x=session.get('id')
        valeurs = Sessionalchemy.query(User)\
            .filter(User.id== x)\
            .all()
        moi = []
        for i in valeurs:
            nom = i.nom
            prenom = i.prenom
            moi.append((prenom, nom))
              
        for i in new_phot:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            new_photos.append(T1)

        
        total = len(new_photos)
        pagination_users=new_photos[offset: offset + per_page]
        pagination = Pagination(page=page, per_page=per_page, total=total,css_framework='bootstrap4')
    
    else:
        return redirect(url_for('login')) 
    
    return render_template('piecevalide.html', users=pagination_users, page=page, per_page=per_page,pagination=pagination,comptes=comptes, moi=moi)

    


@app.route('/chef', methods=['GET', 'POST'])
def chef():
    count=0
    liste = []
    listeUsers = []
    userTrue =[]
    nous=[]
    new_photos=[]
    if session.get('id')==1 or session.get('id')==2 or session.get('id')==4:
        # Selection et affiche des photos dont leurs etats=false
        comptes = Sessionalchemy.query(User)\
            .distinct()\
            .count()
        new_phot = []
        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo== 'invalide')\
            .all()

        for i in valeurs:
            id = i.id
            nom = i.nom
            photo = i.photo
            prix = i.prix
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            adresse = i.adresse
            objet = i.objet
            new_phot.append((id, photo, nom, prix, nomvendeur, telephone, adresse, objet))
        x=session.get('id')
        valeurs = Sessionalchemy.query(User)\
            .filter(User.id== x)\
            .all()
        moi = []
        for i in valeurs:
            nom = i.nom
            prenom = i.prenom
            moi.append((prenom, nom))
              
        for i in new_phot:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            new_photos.append(T1)
    else:
        return redirect(url_for('login')) 

    return render_template('user.html', new_photos=new_photos,comptes=comptes, moi=moi)


@app.route('/valider/<string:id_ap>', methods=['GET'])
def valider(id_ap):
    ids=session.get('id')
    # now = datetime.datetime.now()
    # dd/mm/YY H:M:S
    #datevalid = now.strftime("%d/%m/%Y %H:%M:%S")
    datevalid = str(datetime.datetime.now())

    admin = Sessionalchemy.query(Modele)\
        .filter(Modele.id==id_ap).first()
    admin.etat_photo = 'valide'
    admin.date_valide = datevalid
    admin.user_id = ids
    Sessionalchemy.commit()
    
    return redirect(url_for('chef'))

@app.route('/supprimer/<string:id_ap>', methods=['GET'])
def supprimer(id_ap):
    phot=[]

    admin = Sessionalchemy.query(Modele)\
        .filter(Modele.id==id_ap).first()
    admin.etat_photo = 'adm_supprime'
    Sessionalchemy.commit()
    flash("La commande a été supprimée.","succes")

    phott = []
    valeurs = Sessionalchemy.query(Modele)\
        .filter(Modele.id== id_ap)\
        .all()
    for i in valeurs:
        photo = i.photo
        phott.append(photo)

    return redirect(url_for('chef'))


@app.route('/contact', methods=['GET', 'POST'])
def contact():

    return render_template('contactus.html')

@app.route('/new', methods=['GET', 'POST'])
def new():
    
    return render_template('collectenewvoiture.html')


@app.route('/collectenewmoto', methods=['GET', 'POST'])
def collectenewmoto():
    
    return render_template('collectenewmoto.html')


@app.route('/collectenewpiece', methods=['GET', 'POST'])
def collectenewpiece():
    
    return render_template('collectenewpiece.html')


@app.route('/ajoumodel', methods=['GET', 'POST'])
def ajoumodel():
    im = []
    id_user = session.get('id')
    marque = ""
    annee = 0
    km = 0
    nbr_port = 0
    boitvit = ""
    typecarburant =""
    prixx = 0
    description = ""
    objet =""
    photoo = ""

    if request.method == 'POST': #and 'options' in request.form and 'choix' in request.form :
            # photoo = request.form.get("photo")
            categ = request.form.get("options")
            marque = request.form.get("choix")
            prixx = request.form.get("prix")
            description = request.form.get("description")
            objet = request.form.get("objet")
            if categ == "voiture":
                annee = request.form.get("annee")
                km = request.form.get("kilometre")
                nbr_port = request.form.get("Nombredeportes")
                boitvit = request.form.get("boitevitesse")
                typecarburant = request.form.get("typecarburant")
            elif categ == "moto" or categ == "piece":
                annee = 0
                km = 0
                nbr_port = 0
                boitvit = ""
                typecarburant =""
            # print(nom, marque, annee, km, nbr_port, boitvit, typecarburant, prixx, description)

            use = Sessionalchemy.query(User)\
                .filter(User.id == id_user)\
                .all()
            
            user = []
            for i in use:
                nom = i.nom
                telephone = i.telephone
                adresse = i.adresse
                mail = i.mail
                user.append((nom, telephone, adresse, mail))
            nom = user[0][0] #request.form.get("nomPerson")
            nomPerson=nom
            telephone = user[0][1] #request.form.get("telephone")
            adresse = user[0][2] #request.form.get("adresse")            
            etat_photo = 'invalide' 
            mail = user[0][3]
            dateajout = str(datetime.datetime.now())

            # # *********************************** 


            images = request.files.getlist("imageq")
            if len(images)>2:
                flash("Vous êtes autorisé à télécharger au maximum 2 photos.","danger")
            else:
                for image in images:
                    if image.filename == "":
                        flash("Entrez svp une image", "avertissement")
                        return redirect(request.url)

                    if allowed_image(image.filename):
                        
                        filename = secure_filename(image.filename)
                        
                        filenam, file_extension = os.path.splitext(filename)
                        
                        hash = generate_password_hash(filenam)
                        hash=hash[-10:]
                        d = hash+file_extension
                        im.append(d)    
                        image.save(os.path.join(app.config["IMAGE_UPLOADS"], d))
                if len(im)==2:
                    dd= im[0]+'&-&'+im[1]
                else:
                    dd= im[0]+'&-&'+im[0]
                photoo =dd
                user = Sessionalchemy.query(User).filter_by(id=id_user).first() 
                #user = [val for val in user][0]
                models = Modele(marque,categ, marque, photoo, annee, typecarburant, km, nbr_port, boitvit, prixx, etat_photo, dateajout, None, nomPerson, telephone, mail, adresse, description, objet,user)
                Sessionalchemy.add(models)
                Sessionalchemy.commit()
                nom = request.form.get("options")
                                                
                flash("Felicitation, vous avez ajouté un modéle avec succés, merci de patienter pour la validation de notre équipe.","succes")   

            # else:
            #     return " not in request.cookie" #redirect(request.url)

    
    
    return render_template('ajoumodel.html')


@app.route('/toyota', methods=['GET', 'POST'])
def toyota():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    toyotaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Toyota")\
        .all()

    toyotas = []
    vendeur = []
    for i in toyotaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet

        
        toyotas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    # if len(toyotas) != 0:
    for i in toyotas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(toyotas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Toyota.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/mercedes', methods=['GET', 'POST'])
def mercedes():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    mercedesa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Mercedes-benz")\
        .all()

    mercedess = []
    vendeur = []
    for i in mercedesa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        mercedess.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in mercedess:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(mercedess)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('mercedes.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)



@app.route('/bmw', methods=['GET', 'POST'])
def bmw():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    bmwa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="BMW")\
        .all()

    bmws = []
    vendeur = []
    for i in bmwa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        bmws.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in bmws:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(bmws)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('bmw.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

    

@app.route('/ford', methods=['GET', 'POST'])
def ford():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Forda = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Ford")\
        .all()

    Fords = []
    vendeur = []
    for i in Forda:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Fords.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Fords:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Fords)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('ford.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Honda', methods=['GET', 'POST'])
def Honda():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Hondaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Honda")\
        .all()

    Hondas = []
    vendeur = []
    for i in Hondaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Hondas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Hondas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Hondas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Honda.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/hyundai', methods=['GET', 'POST'])
def hyundai():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    hyundaia = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="hyundai")\
        .all()

    hyundais = []
    vendeur = []
    for i in hyundaia:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        hyundais.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in hyundais:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(hyundais)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('hyundai.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

    
@app.route('/Nissan', methods=['GET', 'POST'])
def Nissan():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Nissana = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Nissan")\
        .all()

    Nissans = []
    vendeur = []
    for i in Nissana:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Nissans.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Nissans:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Nissans)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Nissan.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Renault', methods=['GET', 'POST'])
def Renault():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Renaulta = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Renault")\
        .all()

    Renaults = []
    vendeur = []
    for i in Renaulta:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Renaults.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Renaults:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Renaults)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Renault.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Peugeot', methods=['GET', 'POST'])
def Peugeot():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Peugeota = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Peugeot")\
        .all()

    Peugeots = []
    vendeur = []
    for i in Peugeota:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Peugeots.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Peugeots:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Peugeots)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Peugeot.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Citroen', methods=['GET', 'POST'])
def Citroen():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Citroena = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Citroën C3")\
        .all()

    Citroens = []
    vendeur = []
    for i in Citroena:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Citroens.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Citroens:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Citroens)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Citroen.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Dacia', methods=['GET', 'POST'])
def Dacia():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Daciaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Dacia")\
        .all()

    Dacias = []
    vendeur = []
    for i in Daciaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Dacias.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Dacias:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Dacias)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Dacia.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Mitsubishi', methods=['GET', 'POST'])
def Mitsubishi():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Mitsubishia = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Mitsubishi")\
        .all()

    Mitsubishis = []
    vendeur = []
    for i in Mitsubishia:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Mitsubishis.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Mitsubishis:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Mitsubishis)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Mitsubishi.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Volkswagen', methods=['GET', 'POST'])
def Volkswagen():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Volkswagena = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Volkswagen")\
        .all()

    Volkswagens = []
    vendeur = []
    for i in Volkswagena:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Volkswagens.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Volkswagens:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Volkswagens)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Volkswagen.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/volvo', methods=['GET', 'POST'])
def volvo():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    volvoa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="volvo")\
        .all()

    volvos = []
    vendeur = []
    for i in volvoa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        volvos.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in volvos:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(volvos)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('volvo.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/mazda', methods=['GET', 'POST'])
def mazda():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    mazdaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="mazda")\
        .all()

    mazdas = []
    vendeur = []
    for i in mazdaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        mazdas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in mazdas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(mazdas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('mazda.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/suzuki', methods=['GET', 'POST'])
def suzuki():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    suzukia = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="suzuki")\
        .all()

    suzukis = []
    vendeur = []
    for i in suzukia:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        suzukis.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in suzukis:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(suzukis)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('suzuki.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/kia', methods=['GET', 'POST'])
def kia():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    kiaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="kia")\
        .all()

    kias = []
    vendeur = []
    for i in kiaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        kias.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in kias:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(kias)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('kia.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Autres', methods=['GET', 'POST'])
def Autres():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Autresa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='voiture')\
        .filter(Modele.marque=="Autres")\
        .all()

    Autress = []
    vendeur = []
    for i in Autresa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Autress.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Autress:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Autress)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Autres.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/TMAX', methods=['GET', 'POST'])
def TMAX():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Tmaxa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Tmax")\
        .all()
    Tmaxs = []
    vendeur = []
    for i in Tmaxa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Tmaxs.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Tmaxs:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Tmaxs)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('TMAX.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/SUZUKI', methods=['GET', 'POST'])
def SUZUKI():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Suzukia = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Suzuki")\
        .all()

    Suzukis = []
    vendeur = []
    for i in Suzukia:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Suzukis.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Suzukis:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Suzukis)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('SUZUKI.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Jakarta', methods=['GET', 'POST'])
def Jakarta():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Jakartaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Jakarta")\
        .all()

    Jakartas = []
    vendeur = []
    for i in Jakartaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Jakartas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Jakartas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Jakartas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Jakarta.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Scooter', methods=['GET', 'POST'])
def Scooter():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Scootera = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Scooter")\
        .all()

    Scooters = []
    vendeur = []
    for i in Scootera:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Scooters.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Scooters:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Scooters)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Scooter.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Beverly', methods=['GET', 'POST'])
def Beverly():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Beverlya = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Beverly")\
        .all()

    Beverlys = []
    vendeur = []
    for i in Beverlya:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Beverlys.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Beverlys:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Beverlys)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Beverly.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Yamaha', methods=['GET', 'POST'])
def Yamaha():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Yamahaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Yamaha")\
        .all()

    Yamahas = []
    vendeur = []
    for i in Yamahaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Yamahas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Yamahas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Yamahas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Yamaha.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
 


@app.route('/Piaggio', methods=['GET', 'POST'])
def Piaggio():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Piaggioa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Piaggio")\
        .all()

    Piaggios = []
    vendeur = []
    for i in Piaggioa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Piaggios.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Piaggios:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Piaggios)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Piaggio.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
 

@app.route('/Peageot', methods=['GET', 'POST'])
def Peageot():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Peageota = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Peageot")\
        .all()

    Peageots = []
    vendeur = []
    for i in Peageota:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Peageots.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Peageots:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Peageots)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Peageot.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
 
@app.route('/Hondas', methods=['GET', 'POST'])
def Hondas():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Hondaa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Honda")\
        .all()

    Hondas = []
    vendeur = []
    for i in Hondaa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Hondas.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Hondas:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Hondas)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Honda_moto.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)





@app.route('/Liberty', methods=['GET', 'POST'])
def Liberty():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Libertya = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Liberty")\
        .all()

    Libertys = []
    vendeur = []
    for i in Libertya:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Libertys.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Libertys:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Libertys)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Liberty.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Autress', methods=['GET', 'POST'])
def Autress():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Autressa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='moto')\
        .filter(Modele.marque=="Autres")\
        .all()

    Autresss = []
    vendeur = []
    for i in Autressa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Autresss.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Autresss:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Autresss)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Autress.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Ampoules', methods=['GET', 'POST'])
def Ampoules():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Ampoulesa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Ampoules")\
        .all()

    Ampouless = []
    vendeur = []
    for i in Ampoulesa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Ampouless.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Ampouless:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Ampouless)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Ampoules.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Plaquettes', methods=['GET', 'POST'])
def Plaquettes():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Plaquettesa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Plaquettes de frein")\
        .all()

    Plaquettess = []
    vendeur = []
    for i in Plaquettesa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Plaquettess.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Plaquettess:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Plaquettess)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Plaquettes.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)



@app.route('/Pneus', methods=['GET', 'POST'])
def Pneus():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Pneusa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Pneus")\
        .all()

    Pneuss = []
    vendeur = []
    for i in Pneusa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Pneuss.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Pneuss:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Pneuss)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Pneus.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Carrosserie', methods=['GET', 'POST'])
def Carrosserie():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Carrosseriea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Carrosserie")\
        .all()

    Carrosseries = []
    vendeur = []
    for i in Carrosseriea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Carrosseries.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Carrosseries:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Carrosseries)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Carrosserie.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/Moteur', methods=['GET', 'POST'])
def Moteur():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Moteura = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Moteur")\
        .all()

    Moteurs = []
    vendeur = []
    for i in Moteura:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Moteurs.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Moteurs:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Moteurs)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Moteur.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Climatisation', methods=['GET', 'POST'])
def Climatisation():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Climatisationa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Climatisation")\
        .all()

    Climatisations = []
    vendeur = []
    for i in Climatisationa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Climatisations.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Climatisations:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Climatisations)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Climatisation.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

@app.route('/Direction', methods=['GET', 'POST'])
def Direction():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Directiona = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Direction")\
        .all()

    Directions = []
    vendeur = []
    for i in Directiona:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Directions.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Directions:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Directions)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Direction.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Echappement', methods=['GET', 'POST'])
def Echappement():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Echappementa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Echappement")\
        .all()

    Echappements = []
    vendeur = []
    for i in Echappementa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Echappements.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Echappements:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Echappements)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Echappement.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Embrayage', methods=['GET', 'POST'])
def Embrayage():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Embrayagea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Embrayage")\
        .all()

    Embrayages = []
    vendeur = []
    for i in Embrayagea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Embrayages.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Embrayages:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Embrayages)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Embrayage.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Autres-voiture', methods=['GET', 'POST'])
def Autresvoiture():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Autresvoiturea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecevoiture')\
        .filter(Modele.marque=="Autres")\
        .all()

    Autresvoitures = []
    vendeur = []
    for i in Autresvoiturea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Autresvoitures.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Autresvoitures:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Autresvoitures)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Autres-voiture.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)
    
@app.route('/Echappement-moto', methods=['GET', 'POST'])
def Echappementmoto():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Echappementmotoa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Échappement moto")\
        .all()

    Echappementmotos = []
    vendeur = []
    for i in Echappementmotoa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Echappementmotos.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Echappementmotos:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Echappementmotos)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Echappement-moto.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Freinage', methods=['GET', 'POST'])
def Freinage():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Freinagea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Freinage Moto")\
        .all()

    Freinages = []
    vendeur = []
    for i in Freinagea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Freinages.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Freinages:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Freinages)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Freinage.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Filtresmoto', methods=['GET', 'POST'])
def Filtresmoto():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Filtresmotoa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Filtres moto")\
        .all()

    Filtresmotos = []
    vendeur = []
    for i in Filtresmotoa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Filtresmotos.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Filtresmotos:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Filtresmotos)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Filtresmoto.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Pneus-roue', methods=['GET', 'POST'])
def Pneusroue():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Pneusrouea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Pneus et roues")\
        .all()

    Pneusroues = []
    vendeur = []
    for i in Pneusrouea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Pneusroues.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Pneusroues:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Pneusroues)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Pneus-roue.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Amortisseur', methods=['GET', 'POST'])
def Amortisseur():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Amortisseura = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Amortisseur")\
        .all()

    Amortisseurs = []
    vendeur = []
    for i in Amortisseura:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Amortisseurs.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Amortisseurs:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Amortisseurs)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Amortisseur.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Batteries', methods=['GET', 'POST'])
def Batteries():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Batteriesa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Batteries")\
        .all()

    Batteriess = []
    vendeur = []
    for i in Batteriesa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Batteriess.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Batteriess:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)

                                             
    total = len(Batteriess)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Batteries.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/pieces-moteur', methods=['GET', 'POST'])
def piecesmoteur():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    piecesmoteura = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="pieces moteur et cables")\
        .all()

    piecesmoteurs = []
    vendeur = []
    for i in piecesmoteura:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        piecesmoteurs.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in piecesmoteurs:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(piecesmoteurs)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('pieces-moteur.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


@app.route('/Habillages', methods=['GET', 'POST'])
def Habillages():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Habillagesa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Habillages protection moto")\
        .all()

    Habillagess = []
    vendeur = []
    for i in Habillagesa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Habillagess.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Habillagess:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Habillagess)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Habillages.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

 
@app.route('/Eclairage', methods=['GET', 'POST'])
def Eclairage():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Eclairagea = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Eclairage et signalisation")\
        .all()

    Eclairages = []
    vendeur = []
    for i in Eclairagea:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Eclairages.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Eclairages:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Eclairages)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Eclairage.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)

 
@app.route('/Autres-moto', methods=['GET', 'POST'])
def Autresmoto():
    page = int(request.args.get('page', 1))
    per_page = 20
    offset = (page - 1) * per_page
    Autresmotoa = Sessionalchemy.query(Modele)\
        .filter(Modele.etat_photo=='valide')\
        .filter(Modele.categorie=='piecemoto')\
        .filter(Modele.marque=="Autres")\
        .all()

    Autresmotos = []
    vendeur = []
    for i in Autresmotoa:  
        id = i.id  
        nom = i.nom
        marque = i.marque
        photo = i.photo 
        annee_modele = i.annee_modele 
        type_carburant = i.type_carburant   
        kilometre = i.kilometre   
        nbr_portes =i.nbr_portes
        boite_vitesse = i.boite_vitesse
        prix = i.prix
        nomvendeur = i.nom_vendeur
        telephone = i.telephone
        mail = i.mail
        adresse = i.adresse
        description = i.description
        objet = i.objet
        
        Autresmotos.append((id,nom, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, nomvendeur, telephone, mail, adresse, description, objet))
    for i in Autresmotos:
        L1=list(i)
        L1[3]=L1[3].split('&-&')
        T1=tuple(L1)
        vendeur.append(T1)
    
                                             
    total = len(Autresmotos)
    paginationUsers = vendeur[offset: offset + per_page]
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')
  
    return render_template('Autres-moto.html',users=paginationUsers, page=page, per_page=per_page,pagination=pagination)


 
@app.route('/Uservoiture', methods=['GET', 'POST'])
def Uservoiture():
    photos=[]
    im=[]
    dd=""
    if session.get('id'):
        id_user = session.get('id',None)
        
        nom_user = Sessionalchemy.query(User.nom)\
            .filter_by(id=id_user).first()

        # userr = Sessionalchemy.query(User)\
        #         .all()

        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo=='valide')\
            .filter(Modele.categorie=='voiture')\
            .filter(Modele.user_id==id_user)\
            .all()

        pho = []
        for i in valeurs:
            id = i.id
            marque = i.marque
            annee_modele = i.annee_modele
            type_carburant = i.type_carburant
            kilometre = i.kilometre
            nbr_portes = i.nbr_portes
            boite_vitesse = i.boite_vitesse
            photo = i.photo
            prix = i.prix
            categorie = i.categorie
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            mail = i.mail
            adresse = i.adresse
            # boutique = i.boutique
            pho.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, categorie, nomvendeur, telephone, mail, adresse))

        for i in pho:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            photos.append(T1)
  
    return render_template('uservoiture.html', photos=photos)


@app.route('/Usermoto', methods=['GET', 'POST'])
def Usermoto():
    photos=[]
    im=[]
    dd=""
    if session.get('id'):
        id_user = session.get('id',None)
        
        nom_user = Sessionalchemy.query(User.nom)\
            .filter_by(id=id_user).first()

        # userr = Sessionalchemy.query(User)\
        #         .all()

        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo=='valide')\
            .filter(Modele.categorie=='moto')\
            .filter(Modele.user_id==id_user)\
            .all()

        pho = []
        for i in valeurs:
            id = i.id
            marque = i.marque
            annee_modele = i.annee_modele
            type_carburant = i.type_carburant
            kilometre = i.kilometre
            nbr_portes = i.nbr_portes
            boite_vitesse = i.boite_vitesse
            photo = i.photo
            prix = i.prix
            categorie = i.categorie
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            mail = i.mail
            adresse = i.adresse
            # boutique = i.boutique
            pho.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, categorie, nomvendeur, telephone, mail, adresse))

        for i in pho:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            photos.append(T1)
  
    return render_template('usermoto.html', photos=photos)


@app.route('/Userpiece', methods=['GET', 'POST'])
def Userpiece():
    photos=[]
    im=[]
    dd=""
    if session.get('id'):
        id_user = session.get('id',None)
        
        nom_user = Sessionalchemy.query(User.nom)\
            .filter_by(id=id_user).first()

        # userr = Sessionalchemy.query(User)\
        #         .all()

        valeurs = Sessionalchemy.query(Modele)\
            .filter(Modele.etat_photo=='valide')\
            .filter(Modele.categorie=='piecevoiture')\
            .filter(Modele.user_id==id_user)\
            .all()

        pho = []
        for i in valeurs:
            id = i.id
            marque = i.marque
            annee_modele = i.annee_modele
            type_carburant = i.type_carburant
            kilometre = i.kilometre
            nbr_portes = i.nbr_portes
            boite_vitesse = i.boite_vitesse
            photo = i.photo
            prix = i.prix
            categorie = i.categorie
            nomvendeur = i.nom_vendeur
            telephone = i.telephone
            mail = i.mail
            adresse = i.adresse
            # boutique = i.boutique
            pho.append((id, photo, marque, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, categorie, nomvendeur, telephone, mail, adresse))

        for i in pho:
            L1=list(i)
            L1[1]=L1[1].split('&-&')
            T1=tuple(L1)
            photos.append(T1)
  
    return render_template('userpiece.html', photos=photos)


# @app.route('/vide', methods=['GET', 'POST'])
# def vide():
    
#     return render_template('vide.html')

# @app.route('/collectenewpiece', methods=['GET', 'POST'])
# def collectenewpiece():
    
#     return render_template('collectenewpiece.html')










#########################################  DECONNEXION  ###########################

@app.route('/deconnexion')
def logout():
    # Remove session data, this will log the user out
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('nom_utilisateur', None)
   # Redirect to login page
   return redirect(url_for('login'))

   return render_template('deconnexion.html')

if __name__ == '__main__': #si le fichier est executer alors execute le bloc
    app.run(debug=True) #debug=True relance le serveur à chaque modification


