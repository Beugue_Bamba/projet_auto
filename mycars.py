# coding=utf-8

from sqlalchemy import Column, String, Integer, Numeric, Date, ForeignKey
from sqlalchemy.orm import relationship

from basevoiture import Base

class Modele(Base):
    __tablename__ = 'modeles'

    id = Column(Integer, primary_key=True)
    nom = Column(String)
    categorie = Column(String) #voiture; moto; piece
    marque = Column(String)
    photo = Column(String)
    annee_modele = Column(String)
    type_carburant = Column(String)
    kilometre = Column(String)
    nbr_portes = Column(String)
    boite_vitesse = Column(String) # automatique; manuelle
    prix = Column(Numeric)
    etat_photo = Column(String) # valide; invalide; adm_supprime; user_supprime
    date_ajout = Column(Date)
    date_valide = Column(Date)
    nom_vendeur = Column(String)
    telephone = Column(String)
    mail = Column(String)
    adresse = Column(String)
    description = Column(String)
    objet = Column(String)  # vente, location
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", backref="modeles")


    def __init__(self, nom, categorie, marque, photo, annee_modele, type_carburant, kilometre, nbr_portes, boite_vitesse, prix, etat_photo, date_ajout, date_valide, nom_vendeur, telephone, mail, adresse, description,objet, user):
        self.nom = nom
        self.categorie = categorie
        self.marque = marque
        self.photo = photo
        self.annee_modele = annee_modele
        self.type_carburant = type_carburant
        self.kilometre = kilometre
        self.nbr_portes = nbr_portes
        self.boite_vitesse = boite_vitesse
        self.prix = prix
        self.etat_photo = etat_photo
        self.date_ajout = date_ajout
        self.date_valide = date_valide
        self.nom_vendeur = nom_vendeur
        self.telephone = telephone
        self.mail = mail
        self.adresse = adresse
        self.description = description
        # self.boutique = boutique
        self.user = user
        self.objet = objet
