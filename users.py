# coding=utf-8

from sqlalchemy import Column, String, Integer, Date

from basevoiture import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    prenom = Column(String)
    nom = Column(String)
    # boutique = Column(String)
    telephone = Column(String)
    adresse = Column(String)
    mail = Column(String)
    pass_word = Column(String)
    role = Column(String) # admine; vendeur
    etat = Column(String) #valide ; invalide
    date_inscription = Column(Date)

    def __init__(self,prenom, nom, telephone, adresse, mail, pass_word, role, etat, date_inscription):
        self.prenom = prenom
        self.nom = nom
        # self.boutique = boutique
        self.telephone = telephone
        self.adresse = adresse
        self.mail = mail
        self.pass_word = pass_word
        self.role = role
        self.etat = etat
        self.date_inscription = date_inscription
